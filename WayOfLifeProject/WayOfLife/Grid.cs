﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WayOfLife
{
    /// <summary>
    /// Classe contenant une grille d'une taille définie.
    /// </summary>
    public class Grid
    {
        private int[,] values;
        public int[,] Values { get { return values; } }
        public int Width { get { return Values.GetLength(0); } }
        public int Height { get { return Values.GetLength(1); } }

        public Grid(int width, int height)
        {
            values = new int[width, height];
        }

        /// <summary>
        /// Transforme la grille en chaîne de caractères.
        /// </summary>
        /// <returns>Renvoie une chaîne de caractères.</returns>
        public override string ToString()
        {
            try
            {
                string value;
                int counter = 0;
                value = "";
                for (int i = 0; i < Width; i++)
                {
                    for (int j = 0; j < Height; j++)
                    {
                        counter += Values[i, j];
                        value += Values[i, j];
                    }
                    value += "\n";
                }
                if (counter <= 0)
                {
                    throw new EmptyGridException(this, "Vous ne pouvez pas sauvegarder une grille vide.");
                }
                return value;
            }
            catch(Exception ex)
            {
                if (ex.GetType() == typeof(EmptyGridException))
                {
                    throw ex;
                }
                else
                {
                    throw new InvalidGridSizeException(this, ex.Message, ex.InnerException);
                }
            }
        }

        /// <summary>
        /// Transforme une chaîne de caractères en grille.
        /// </summary>
        /// <param name="gridText">Chaîne de caractères à convertir en grille.</param>
        public void loadGrid(string gridText)
        {
            try
            {
                string[] newValues;
                newValues = gridText.Split('\n');
                for (int i = 0; i < newValues.Length; i++)
                {
                    for (int j = 0; j < newValues[i].Length; j++)
                    {
                        setValue(i, j, (int)Char.GetNumericValue(newValues[i][j]));
                    }
                }
            }
            catch(Exception ex)
            {
                throw new InvalidGridSizeException(this, ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Définie une valeur à la position voulue dans la grille.
        /// </summary>
        /// <param name="x">Position voulue dans l'axe X.</param>
        /// <param name="y">Position voulue dans l'axe Y.</param>
        /// <param name="value">Valeur voulue.</param>
        public void setValue(int x, int y, int value)
        {
            values[x, y] = value;
        }
    }

    /// <summary>
    /// Exception signifiant que la grille concernée possède une taille invalide.
    /// </summary>
    public class InvalidGridSizeException : Exception
    {
        public Grid concernedGrid;

        public InvalidGridSizeException(Grid grid) : base()
        {
            concernedGrid = grid;
        }

        public InvalidGridSizeException(Grid grid, string message) : base(message)
        {
            concernedGrid = grid;
        }

        public InvalidGridSizeException(Grid grid, string message, Exception inner) : base(message, inner)
        {
            concernedGrid = grid;
        }
    }

    /// <summary>
    /// Exception signifiant que la grille concernée est vide.
    /// </summary>
    public class EmptyGridException : Exception
    {
        public Grid concernedGrid;

        public EmptyGridException(Grid grid) : base()
        {
            concernedGrid = grid;
        }

        public EmptyGridException(Grid grid, string message) : base(message)
        {
            concernedGrid = grid;
        }

        public EmptyGridException(Grid grid, string message, Exception inner) : base(message, inner)
        {
            concernedGrid = grid;
        }
    }
}
