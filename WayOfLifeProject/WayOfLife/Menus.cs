﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WayOfLife
{
    /// <summary>
    /// Classe gérant les menus.
    /// </summary>
    public static class Menus
    {
        public enum Screen//Liste des menus
        {
            None,
            Title,
            Play,
            Pause,
            Options,
            Editor,
            Save
        }

        private static Screen lastScreen;
        public static Screen LastScreen { get { return lastScreen; } }//indicateur du menu précédent à l'appui d'un bouton
        private static Screen currentScreen;
        public static Screen CurrentScreen { get { return currentScreen; } }//indicateur du menu actuel

        private static bool pause;
        public static bool Pause { get { return pause; } }//état du jeu

        /// <summary>
        /// Fonction permettant le changement d'écran
        /// </summary>
        /// <param name="wantedMenu"></param>
        public static void SwitchScreen(Screen wantedMenu)
        {
            lastScreen = currentScreen;
            currentScreen = wantedMenu;
        }

        /// <summary>
        /// Met en pause l'état du jeu.
        /// </summary>
        public static void PauseUpdate()
        {
            pause = true;
        }

        /// <summary>
        /// Sort de pause l'état du jeu.
        /// </summary>
        public static void UnpauseUpdate()
        {
            pause = false;
        }
    }
}
