﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Linq;

namespace WayOfLife
{
    public partial class MainForm : Form
    {
        #region Variables
        private List<Virus> virusList;
        private Grid grid;//grille actuelle
        private Grid lastGrid;//grille précédente
        private Grid gridAlive;//Nombre d'actualisation sans changements dans la grille
        private bool[,,] shapesGrid;//Liste des formes pré-définies sous forme de cases
        private int[] countAlive;//Nombre de cases vivantes autour d'une case
        private int selectedShape;
        #endregion

        /// <summary>
        /// Initialisation et mise en place des paramètres et valeurs de base ainsi que la fenêtre de jeu.
        /// </summary>
        public MainForm()
        {
            //Initialise tous les composants posés de façon graphique
            InitializeComponent();
            //Création des virus
            virusList = new List<Virus>();
            virusList.Add(new Virus("NoVirus", Color.FromArgb(250, 250, 250), 0));
            virusList.Add(new Virus("Conway", Color.FromArgb(150, 150, 150), 1));//Virus de base du jeu de la vie
            virusList.Add(new Virus("FireConway", Color.FromArgb(250, 0, 0), 2));//Virus de base V2
            virusList.Add(new Virus("PlantConway", Color.FromArgb(0, 250, 0), 3));//Virus de base V2
            virusList.Add(new Virus("WaterConway", Color.FromArgb(0, 0, 250), 4));//Virus de base V2
            virusList.Add(new Virus("Eater", Color.FromArgb(200, 0, 150), 5));//Virus mangeur de virus
            //Ajout des virus à la liste de l'éditeur
            foreach (Virus each in virusList.Skip(1))
            {
                VirusChooser.Items.Add(each.Name);
            }
            VirusChooser.SelectedIndex = 0;
            //Création de la liaison des couleurs et des virus
            countAlive = new int[6];
            //Dessin de la grille
            for (int x = 0; x < MainBox.Image.Width / 10 * 10; x += 1)
            {
                for (int y = 10; y < MainBox.Image.Height / 10 * 10; y += 10)
                {
                    Color newColor = Color.FromArgb(200, 200, 200);
                    ((Bitmap)(MainBox.Image)).SetPixel(x, y, newColor);
                }
            }
            for (int x = 10; x < MainBox.Image.Width / 10 * 10; x += 10)
            {
                for (int y = 0; y < MainBox.Image.Height / 10 * 10; y += 1)
                {
                    Color newColor = Color.FromArgb(200, 200, 200);
                    ((Bitmap)(MainBox.Image)).SetPixel(x, y, newColor);
                }
            }
            //Mise en place d'une grille "en code" de la taille de l'écran
            grid = new Grid(MainBox.Image.Width / 10, MainBox.Image.Height / 10);
            lastGrid = new Grid(MainBox.Image.Width / 10, MainBox.Image.Height / 10);
            gridAlive = new Grid(MainBox.Image.Width / 10, MainBox.Image.Height / 10);
            //Coloriage des cellules et valeurs de base des grilles
            for (int i = 0; i < grid.Width; i++)
            {
                for (int j = 0; j < grid.Height; j++)
                {
                    lastGrid.setValue(i, j, grid.Values[i, j]);
                    gridAlive.setValue(i, j, 0);

                    Color newColor;
                    if (lastGrid.Values[i, j] > 0)
                    {
                        newColor = virusList[grid.Values[i, j]].Color;
                    }
                    else
                    {
                        newColor = virusList[0].Color;
                    }
                    using (Graphics g = Graphics.FromImage(MainBox.Image))
                    {
                        g.FillRectangle(new SolidBrush(newColor), i * 10 + 1, j * 10 + 1, 9, 9);
                    }
                }
            }
            //Formes pré-définies pour l'éditeur
            PictureBox[] ShapesList = new PictureBox[11];
            shapesGrid = new bool[11, 5, 5];
            for (int i = 0; i < ShapesList.Length; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    for (int k = 0; k < 5; k++)
                    {
                        shapesGrid[i, j, k] = false;
                    }
                }
            }
            #region Planeur
            //Première forme
            shapesGrid[0, 1, 1] = true;
            shapesGrid[0, 1, 3] = true;
            shapesGrid[0, 2, 2] = true;
            shapesGrid[0, 2, 3] = true;
            shapesGrid[0, 3, 2] = true;
            //Deuxième forme
            shapesGrid[1, 1, 1] = true;
            shapesGrid[1, 1, 2] = true;
            shapesGrid[1, 2, 2] = true;
            shapesGrid[1, 2, 3] = true;
            shapesGrid[1, 3, 1] = true;
            //Troisième forme
            shapesGrid[2, 1, 2] = true;
            shapesGrid[2, 2, 1] = true;
            shapesGrid[2, 2, 2] = true;
            shapesGrid[2, 3, 1] = true;
            shapesGrid[2, 3, 3] = true;
            //Quatrième forme
            shapesGrid[3, 1, 3] = true;
            shapesGrid[3, 2, 1] = true;
            shapesGrid[3, 2, 2] = true;
            shapesGrid[3, 3, 2] = true;
            shapesGrid[3, 3, 3] = true;
            #endregion
            #region Départ de floraison
            //Cinquième forme
            shapesGrid[4, 0, 2] = true;
            shapesGrid[4, 1, 1] = true;
            shapesGrid[4, 1, 3] = true;
            shapesGrid[4, 2, 1] = true;
            shapesGrid[4, 2, 2] = true;
            shapesGrid[4, 2, 3] = true;
            shapesGrid[4, 3, 1] = true;
            shapesGrid[4, 3, 3] = true;
            shapesGrid[4, 4, 2] = true;
            //Sixième forme
            shapesGrid[5, 1, 1] = true;
            shapesGrid[5, 1, 2] = true;
            shapesGrid[5, 1, 3] = true;
            shapesGrid[5, 2, 0] = true;
            shapesGrid[5, 2, 2] = true;
            shapesGrid[5, 2, 4] = true;
            shapesGrid[5, 3, 1] = true;
            shapesGrid[5, 3, 2] = true;
            shapesGrid[5, 3, 3] = true;
            #endregion
            #region Bloc
            //Septième forme
            shapesGrid[6, 2, 2] = true;
            shapesGrid[6, 2, 3] = true;
            shapesGrid[6, 3, 2] = true;
            shapesGrid[6, 3, 3] = true;
            #endregion
            #region Grenouille
            //Huitième forme
            shapesGrid[7, 1, 3] = true;
            shapesGrid[7, 2, 2] = true;
            shapesGrid[7, 2, 3] = true;
            shapesGrid[7, 3, 2] = true;
            shapesGrid[7, 3, 3] = true;
            shapesGrid[7, 4, 2] = true;
            //Neuvième forme
            shapesGrid[8, 1, 2] = true;
            shapesGrid[8, 2, 2] = true;
            shapesGrid[8, 2, 3] = true;
            shapesGrid[8, 3, 2] = true;
            shapesGrid[8, 3, 3] = true;
            shapesGrid[8, 4, 3] = true;
            //Dixième forme
            shapesGrid[9, 2, 1] = true;
            shapesGrid[9, 2, 2] = true;
            shapesGrid[9, 2, 3] = true;
            shapesGrid[9, 3, 2] = true;
            shapesGrid[9, 3, 3] = true;
            shapesGrid[9, 3, 4] = true;
            //Onzième forme
            shapesGrid[10, 2, 2] = true;
            shapesGrid[10, 2, 3] = true;
            shapesGrid[10, 2, 4] = true;
            shapesGrid[10, 3, 1] = true;
            shapesGrid[10, 3, 2] = true;
            shapesGrid[10, 3, 3] = true;
            #endregion
            //Dessin des formes
            for (int i = 0; i < ShapesList.Length; i++)
            {
                ShapesList[i] = new PictureBox();
                ShapesList[i].Name = "ShapePicture" + i;
                ShapesList[i].Location = new Point(10 + 60 * i, 10);
                ShapesList[i].Image = new Bitmap(50, 50);
                ShapesList[i].Size = new Size(50, 50);
                for (int j = 0; j < 5; j++)
                {
                    for (int k = 0; k < 5; k++)
                    {
                        if (shapesGrid[i, j, k])
                        {
                            using (Graphics g = Graphics.FromImage(ShapesList[i].Image))
                            {
                                g.FillRectangle(new SolidBrush(virusList[VirusChooser.SelectedIndex + 1].Color), j * 10 + 1, k * 10 + 1, 9, 9);
                            }
                        }
                    }
                }
                ShapesList[i].MouseClick += ShapePicture_MouseClick;
                ShapesList[i].PreviewKeyDown += MainForm_PreviewKeyDown;
                ShapesPanel.Controls.Add(ShapesList[i]);
            }
            selectedShape = -1;
            //Mise en place des tailles de fenêtre disponible
            ComboScreenSize.Items.Add("1280x720");
            ComboScreenSize.Items.Add("1920x1080");
            ComboScreenSize.SelectedItem = "1280x720";
            //Valeur pour une case vide
            countAlive[0] = 0;
            //Form prend en compte les inputs
            this.KeyPreview = true;
        }

        /// <summary>
        /// Evénement lancé à l'apparition de la fenêtre.
        /// Permet de charger le menu "Titre".
        /// </summary>
        /// <param name="sender">Fenêtre</param>
        /// <param name="e">Paramètres de la fenêtre</param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            //déplacement vers l'écran titre
            switchScreen(Menus.Screen.Title);
        }

        /// <summary>
        /// Evénement lancé quand une touche est appuyée.
        /// Lancé uniquement quand une forme pré-définie a été sélectionnée.
        /// </summary>
        /// <param name="sender">Fenêtre</param>
        /// <param name="e">Paramètres des touches appuyées</param>
        private void MainForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //Réaction à l'appui d'une touche
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    //Désélectionne la forme pré-définie
                    selectedShape = -1;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Evénement lancé toutes les 100 ms.
        /// Permet de mettre à jour la grille.
        /// </summary>
        /// <param name="sender">Timer</param>
        /// <param name="e">Paramètres du timer</param>
        private void TimeCounter_Tick(object sender, EventArgs e)
        {
            //Calculs des cases
            for (int i = 0; i < grid.Width; i++)
            {
                for (int j = 0; j < grid.Height; j++)
                {
                    for (int k = 1; k < 6; k++)
                    {
                        //Vérifie le nombre de cellules vivantes autour par type de virus
                        countAlive[k] = 0;
                        if (i > 0) countAlive[k] += lastGrid.Values[i - 1, j] == k ? 1 : 0;
                        if (j > 0) countAlive[k] += lastGrid.Values[i, j - 1] == k ? 1 : 0;
                        if (i > 0 && j > 0) countAlive[k] += lastGrid.Values[i - 1, j - 1] == k ? 1 : 0;
                        if (i < grid.Width - 1) countAlive[k] += lastGrid.Values[i + 1, j] == k ? 1 : 0;
                        if (j < grid.Height - 1) countAlive[k] += lastGrid.Values[i, j + 1] == k ? 1 : 0;
                        if (i < grid.Width - 1 && j > 0) countAlive[k] += lastGrid.Values[i + 1, j - 1] == k ? 1 : 0;
                        if (i > 0 && j < grid.Height - 1) countAlive[k] += lastGrid.Values[i - 1, j + 1] == k ? 1 : 0;
                        if (i < grid.Width - 1 && j < grid.Height - 1) countAlive[k] += lastGrid.Values[i + 1, j + 1] == k ? 1 : 0;
                    }
                    //Mise à jour de la grille
                    switch (grid.Values[i, j])
                    {
                        //Fonctionnement de chaques virus
                        #region Eater virus
                        case 5:
                            switch (countAlive[virusList[5].Value])
                            {
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                    grid.setValue(i, j, 0);
                                    break;
                                default:
                                    break;
                            }
                            if (gridAlive.Values[i, j] > 25)
                            {
                                grid.setValue(i, j, 0);
                            }
                            break;
                        #endregion
                        #region WaterConway
                        case 4:
                            switch (countAlive[virusList[4].Value])
                            {
                                case 2:
                                case 3:
                                    break;
                                default:
                                    grid.setValue(i, j, 0);
                                    break;
                            }
                            switch (countAlive[virusList[1].Value])
                            {
                                case 1:
                                case 2:
                                    grid.setValue(i, j, virusList[1].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[5].Value])
                            {
                                case 0:
                                    break;
                                default:
                                    grid.setValue(i, j, virusList[5].Value);
                                    break;
                            }
                            break;
                        #endregion
                        #region PlantConway
                        case 3:
                            switch (countAlive[virusList[3].Value])
                            {
                                case 2:
                                case 3:
                                    break;
                                default:
                                    grid.setValue(i, j, 0);
                                    break;
                            }
                            switch (countAlive[virusList[2].Value])
                            {
                                case 1:
                                case 2:
                                    grid.setValue(i, j, virusList[2].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[5].Value])
                            {
                                case 0:
                                    break;
                                default:
                                    grid.setValue(i, j, virusList[5].Value);
                                    break;
                            }
                            break;
                        #endregion
                        #region FireConway
                        case 2:
                            switch (countAlive[virusList[2].Value])
                            {
                                case 2:
                                case 3:
                                    break;
                                default:
                                    grid.setValue(i, j, 0);
                                    break;
                            }
                            switch (countAlive[virusList[4].Value])
                            {
                                case 1:
                                case 2:
                                    grid.setValue(i, j, virusList[4].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[5].Value])
                            {
                                case 0:
                                    break;
                                default:
                                    grid.setValue(i, j, virusList[5].Value);
                                    break;
                            }
                            break;
                        #endregion
                        #region Conway
                        case 1:
                            switch (countAlive[virusList[1].Value])
                            {
                                case 2:
                                case 3:
                                    break;
                                default:
                                    grid.setValue(i, j, 0);
                                    break;
                            }
                            switch (countAlive[virusList[3].Value])
                            {
                                case 1:
                                case 2:
                                    grid.setValue(i, j, virusList[3].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch(countAlive[virusList[5].Value])
                            {
                                case 0:
                                    break;
                                default:
                                    grid.setValue(i, j, virusList[5].Value);
                                    break;
                            }
                            break;
                        #endregion
                        #region No Virus
                        case 0:
                            switch (countAlive[virusList[1].Value])
                            {
                                case 3:
                                    grid.setValue(i, j, virusList[1].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[2].Value])
                            {
                                case 3:
                                    grid.setValue(i, j, virusList[2].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[3].Value])
                            {
                                case 3:
                                    grid.setValue(i, j, virusList[3].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[4].Value])
                            {
                                case 3:
                                    grid.setValue(i, j, virusList[4].Value);
                                    break;
                                default:
                                    break;
                            }
                            switch (countAlive[virusList[5].Value])
                            {
                                case 4:
                                    grid.setValue(i, j, virusList[5].Value);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        #endregion
                        default:
                            break;
                    }
                }
            }
            //Mise à jour de l'image de la grille
            for (int i = 0; i < grid.Width; i++)
            {
                for (int j = 0; j < grid.Height; j++)
                {
                    if (grid.Values[i, j] != lastGrid.Values[i, j])
                    {
                        Color newColor;
                        if (grid.Values[i, j] > 0)
                        {
                            newColor = virusList[grid.Values[i, j]].Color;
                        }
                        else
                        {
                            newColor = virusList[0].Color;
                        }
                        using (Graphics g = Graphics.FromImage(MainBox.Image))
                        {
                            g.FillRectangle(new SolidBrush(newColor), i * 10 + 1, j * 10 + 1, 9, 9);
                        }
                        gridAlive.setValue(i, j, 0);
                    }
                    else
                    {
                        gridAlive.setValue(i, j, gridAlive.Values[i, j]);
                    }
                }
            }
            //Copie de la grille actuelle sur la grille précédente
            for (int i = 0; i < grid.Width; i++)
            {
                for (int j = 0; j < grid.Height; j++)
                {
                    lastGrid.setValue(i, j, grid.Values[i, j]);
                }
            }
            //Actualisation de l'affichage
            Refresh();
        }

        #region Non-event functions
        /// <summary>
        /// Fonction de changement de menu.
        /// </summary>
        /// <param name="wantedMenu">Menu voulu</param>
        private void switchScreen(Menus.Screen wantedMenu)
        {
            Menus.SwitchScreen(wantedMenu);
            selectedShape = -1;
            //Désactivation des boutons
            PlayButton.Visible = false; PlayButton.Enabled = false;
            EditorButton.Visible = false; EditorButton.Enabled= false;
            OptionsButton.Visible = false; OptionsButton.Enabled = false;
            QuitButton.Visible = false; QuitButton.Enabled = false;
            PauseButton.Visible = false; PauseButton.Enabled = false;
            SaveMenu.Visible = false; SaveMenu.Enabled = false;
            SaveGrid.Visible = false; SaveGrid.Enabled = false;
            LoadGrid.Visible = false; LoadGrid.Enabled = false;
            EmptyGrid.Visible = false; EmptyGrid.Enabled = false;
            //Désactivation des autres éléments
            MainBox.Visible = false; MainBox.Enabled = false;
            SavesList.Visible = false; SavesList.Enabled = false;
            WindowedRadio.Visible = false; WindowedRadio.Enabled = false;
            FullRadio.Visible = false; FullRadio.Visible = false;
            ComboScreenSize.Visible = false; ComboScreenSize.Enabled = false;
            VirusChooser.Visible = false; VirusChooser.Enabled = false;
            ShapesPanel.Visible = false; ShapesPanel.Enabled = false;
            ErrorLabel.Visible = false; ErrorLabel.Enabled = false;
            //Récupération de la taille actuelle de la fenêtre
            int widthWindow = Int32.Parse(ComboScreenSize.SelectedItem.ToString().Split('x')[0]);
            int heightWindow = Int32.Parse(ComboScreenSize.SelectedItem.ToString().Split('x')[1]);
            //Sélection du menu pour activation et déplacement des éléments nécessaires
            switch (Menus.CurrentScreen)
            {
                case Menus.Screen.Title:
                    PlayButton.Visible = true; PlayButton.Enabled = true;
                    PlayButton.Location = new Point((widthWindow - PlayButton.Width) / 2, (heightWindow - PlayButton.Height * 10) / 2);
                    EditorButton.Visible = true; EditorButton.Enabled = true;
                    EditorButton.Location = new Point((widthWindow - EditorButton.Width) / 2, (heightWindow - EditorButton.Height * 7) / 2);
                    OptionsButton.Visible = true; OptionsButton.Enabled = true;
                    OptionsButton.Location = new Point((widthWindow - OptionsButton.Width) / 2, (heightWindow - OptionsButton.Height * 4) / 2);
                    OptionsButton.Text = "Options";
                    QuitButton.Visible = true; QuitButton.Enabled = true;
                    QuitButton.Location = new Point((widthWindow - QuitButton.Width) / 2, (heightWindow - QuitButton.Height) / 2);
                    Menus.PauseUpdate();
                    break;
                case Menus.Screen.Play:
                    PauseButton.Visible = true; PauseButton.Enabled = true;
                    PauseButton.Location = new Point(widthWindow - (int)(PauseButton.Width * 1.5f), PauseButton.Height);
                    PauseButton.Text = "Pause";
                    MainBox.Visible = true; MainBox.Enabled = true;
                    Menus.UnpauseUpdate();
                    TimeCounter.Start();
                    break;
                case Menus.Screen.Editor:
                    PlayButton.Visible = true; PlayButton.Enabled = true;
                    PlayButton.Location = new Point(widthWindow - (int)(PlayButton.Width * 1.5f), PlayButton.Height * 2);
                    OptionsButton.Visible = true; OptionsButton.Enabled = true;
                    OptionsButton.Location = new Point(widthWindow - OptionsButton.Width * 3, OptionsButton.Height * 2);
                    OptionsButton.Text = "Options";
                    SaveMenu.Visible = true; SaveMenu.Enabled = true;
                    SaveMenu.Location = new Point(widthWindow - (int)(SaveMenu.Width * 2.5f), (int)(SaveMenu.Height * 0.5f));
                    EmptyGrid.Visible = true; EmptyGrid.Enabled = true;
                    EmptyGrid.Location = new Point(EmptyGrid.Width, EmptyGrid.Height * 2);
                    VirusChooser.Visible = true; VirusChooser.Enabled = true;
                    VirusChooser.Location = new Point((int)(VirusChooser.Width * 0.5f), (int)(VirusChooser.Height * 0.5f));
                    ShapesPanel.Visible = true; ShapesPanel.Enabled = true;
                    ShapesPanel.Location = new Point((int)(EmptyGrid.Width * 2.5f), (int)(EmptyGrid.Height * 0.5f));
                    ShapesPanel.Height = EmptyGrid.Height * 3;
                    ShapesPanel.Width = widthWindow - EmptyGrid.Width * 6;
                    MainBox.Visible = true; MainBox.Enabled = true;
                    Menus.PauseUpdate();
                    break;
                case Menus.Screen.Save:
                    PlayButton.Visible = true; PlayButton.Enabled = true;
                    PlayButton.Location = new Point((widthWindow - PlayButton.Width) / 2, (heightWindow - PlayButton.Height * 10) / 2);
                    EditorButton.Visible = true; EditorButton.Enabled = true;
                    EditorButton.Location = new Point((widthWindow - EditorButton.Width) / 2, (heightWindow - EditorButton.Height * 7) / 2);
                    OptionsButton.Visible = true; OptionsButton.Enabled = true;
                    OptionsButton.Location = new Point((widthWindow - OptionsButton.Width) / 2, (heightWindow - OptionsButton.Height * 4) / 2);
                    OptionsButton.Text = "Options";
                    QuitButton.Visible = true; QuitButton.Enabled = true;
                    QuitButton.Location = new Point((widthWindow - QuitButton.Width) / 2, (heightWindow - QuitButton.Height) / 2);
                    SaveGrid.Visible = true; SaveGrid.Enabled = true;
                    SaveGrid.Location = new Point(widthWindow / 2 + (int)(SaveGrid.Width * 4.5f), (heightWindow - SaveGrid.Height * 10) / 2);
                    LoadGrid.Visible = true; LoadGrid.Enabled = true;
                    LoadGrid.Location = new Point(widthWindow / 2 + (int)(LoadGrid.Width * 4.5f), (heightWindow - LoadGrid.Height * 7) / 2);
                    SavesList.Visible = true; SavesList.Enabled = true;
                    SavesList.Location = new Point(widthWindow / 2 + (int)(SavesList.Width * 0.5f), (heightWindow - SavesList.Height) / 2);
                    ErrorLabel.Location = new Point(widthWindow / 2 + 375, (heightWindow + 20) / 2);
                    Menus.PauseUpdate();
                    refreshSavesList();
                    break;
                case Menus.Screen.Options:
                    QuitButton.Visible = true; QuitButton.Enabled = true;
                    QuitButton.Location = new Point(widthWindow / 2 + QuitButton.Width, heightWindow / 2 + QuitButton.Height);
                    OptionsButton.Visible = true; OptionsButton.Enabled = true;
                    OptionsButton.Location = new Point(widthWindow / 2 - OptionsButton.Width, heightWindow / 2 + OptionsButton.Height);
                    OptionsButton.Text = "Retour";
                    WindowedRadio.Visible = true; WindowedRadio.Enabled = true;
                    WindowedRadio.Location = new Point(widthWindow / 2 - WindowedRadio.Width, heightWindow / 2 - WindowedRadio.Height * 3);
                    FullRadio.Visible = true; FullRadio.Enabled = true;
                    FullRadio.Location = new Point(widthWindow / 2 + FullRadio.Width, heightWindow / 2 - FullRadio.Height * 3);
                    ComboScreenSize.Visible = true; ComboScreenSize.Enabled = true;
                    ComboScreenSize.Location = new Point((widthWindow - ComboScreenSize.Width) / 2, heightWindow / 2 - ComboScreenSize.Height);
                    Menus.PauseUpdate();
                    if (FullRadio.Checked)
                    {
                        ComboScreenSize.Enabled = false;
                    }
                    break;
                case Menus.Screen.Pause:
                    EditorButton.Visible = true; EditorButton.Enabled = true;
                    EditorButton.Location = new Point((widthWindow - EditorButton.Width) / 2, (heightWindow - EditorButton.Height * 7) / 2);
                    OptionsButton.Visible = true; OptionsButton.Enabled = true;
                    OptionsButton.Location = new Point((widthWindow - OptionsButton.Width) / 2, (heightWindow - OptionsButton.Height) / 2);
                    OptionsButton.Text = "Options";
                    QuitButton.Visible = true; QuitButton.Enabled = true;
                    QuitButton.Location = new Point((widthWindow - QuitButton.Width) / 2, (heightWindow + QuitButton.Height * 2) / 2);
                    PauseButton.Visible = true; PauseButton.Enabled = true;
                    PauseButton.Location = new Point((widthWindow - PauseButton.Width) / 2, (heightWindow - PauseButton.Height * 10) / 2);
                    PauseButton.Text = "Reprendre";
                    SaveMenu.Visible = true; SaveMenu.Enabled = true;
                    SaveMenu.Location = new Point((widthWindow - SaveMenu.Width) / 2, (heightWindow - SaveMenu.Height * 4) / 2);
                    Menus.PauseUpdate();
                    break;
                default:
                    break;
            }
            //Actualisation de l'affichage
            Refresh();
        }

        /// <summary>
        /// Fonction permettant de convertir un string en grille.
        /// </summary>
        /// <param name="gridValue">string à convertir</param>
        private void stringToGrid(string gridValue)
        {
            grid.loadGrid(gridValue);
            for (int i = 0; i < grid.Width; i++)
            {
                for (int j = 0; j < grid.Height; j++)
                {
                    if (grid.Values[i, j] != lastGrid.Values[i, j])
                    {
                        Color newColor;
                        if (grid.Values[i, j] > 0)
                        {
                            newColor = virusList[grid.Values[i, j]].Color;
                        }
                        else
                        {
                            newColor = virusList[0].Color;
                        }
                        using (Graphics g = Graphics.FromImage(MainBox.Image))
                        {
                            g.FillRectangle(new SolidBrush(newColor), i * 10 + 1, j * 10 + 1, 9, 9);
                        }
                    }
                    lastGrid.setValue(i, j, grid.Values[i, j]);
                    gridAlive.setValue(i, j, 0);
                }
            }
        }

        /// <summary>
        /// Fonction permettant de mettre à jour la liste des fichiers dans la liste de sauvegardes.
        /// </summary>
        private void refreshSavesList()
        {
            if (Menus.CurrentScreen == Menus.Screen.Save)
            {
                SavesList.Items.Clear();
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife"));
                string[] files = Directory.GetFiles(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife"));
                foreach (string file in files)
                {
                    string fileName = Path.GetFileNameWithoutExtension(file);
                    ListViewItem item = new ListViewItem(fileName);
                    item.Tag = file;
                    //Ajout du nom du fichier à la liste
                    SavesList.Items.Add(item);
                }
            }
        }
        #endregion

        #region Buttons click
        /// <summary>
        /// Evénement lancé à l'appui du bouton "Jouer".
        /// Permet de charger le menu de jeu.
        /// </summary>
        /// <param name="sender">Bouton "Jouer"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void PlayButton_Click(object sender, EventArgs e)
        {
            switchScreen(Menus.Screen.Play);
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Editeur".
        /// Permet de charger le menu d'édition.
        /// </summary>
        /// <param name="sender">Bouton "Editeur"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void EditorButton_Click(object sender, EventArgs e)
        {
            switchScreen(Menus.Screen.Editor);
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Options" et "Retour".
        /// Permet de charger le menu d'options ou le quitter si on y est déjà.
        /// </summary>
        /// <param name="sender">Bouton "Options" et "Retour"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void OptionsButton_Click(object sender, EventArgs e)
        {
            if (Menus.CurrentScreen == Menus.Screen.Options)
            {
                switchScreen(Menus.LastScreen);
            }
            else
            {
                switchScreen(Menus.Screen.Options);
            }
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Quitter".
        /// Permet de quitter l'application.
        /// </summary>
        /// <param name="sender">Bouton "Quitter"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void QuitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Pause" ou "Reprendre".
        /// Permet de mettre en pause la partie ou la reprendre si elle est en pause.
        /// </summary>
        /// <param name="sender">Bouton "Pause" ou "Reprendre"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void PauseButton_Click(object sender, EventArgs e)
        {
            TimeCounter.Stop();
            if (Menus.Pause)
            {
                switchScreen(Menus.Screen.Play);
            }
            else
            {
                switchScreen(Menus.Screen.Pause);
            }
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Sauver/Charger".
        /// Permet d'afficher le menu des sauvegardes.
        /// </summary>
        /// <param name="sender">Bouton "Sauver/Charger"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void SaveMenu_Click(object sender, EventArgs e)
        {
            switchScreen(Menus.Screen.Save);
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Sauvegarder".
        /// Permet de sauvegarder la grille actuelle dans un fichier ".txt".
        /// </summary>
        /// <param name="sender">Bouton "Sauvegarder"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void SaveGrid_Click(object sender, EventArgs e)
        {
            bool empty = false;
            try
            {
                //Sauvegarde la grille dans un fichier ".txt" dans "Appdata\Roaming\WayOfLife\"
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife"));
                using (StreamWriter sw = File.CreateText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife\save" + (Directory.GetFiles(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife")).Length + 1).ToString() + ".txt")))
                {
                    empty = true;
                    sw.Write(grid.ToString());
                    empty = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(InvalidGridSizeException))
                {
                    //Taille de la grille invalide, cette erreur ne devrait jamais arriver à moins qu'un problème soit présent dans le code
                    ErrorLabel.Visible = true; ErrorLabel.Enabled = true;
                    ErrorLabel.Text = "Une erreur est survenue dans le code. Veuillez soumettre une issue avec le code (0x01) au dévelopeur.";
                }
                else
                {
                    //Sauvegarde échouée
                    ErrorLabel.Visible = true; ErrorLabel.Enabled = true;
                    ErrorLabel.Text = ex.Message;
                }
            }
            finally
            {
                //Supprime le fichier créé car il est vide
                if (empty) File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife\save" + (Directory.GetFiles(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife")).Length).ToString() + ".txt"));
                //Met à jour la liste des sauvegardes
                refreshSavesList();
            }
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Charger".
        /// Permet de charger une grille sauvegardée dans un fichier ".txt" qui est sélectionnée dans la liste.
        /// </summary>
        /// <param name="sender">Bouton "Charger"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void LoadGrid_Click(object sender, EventArgs e)
        {
            try
            {
                //Charge la grille de la sauvegarde sélectionnée dans la liste
                if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife\" + SavesList.SelectedItems[0].Text + ".txt")))
                {
                    string text = File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"WayOfLife\" + SavesList.SelectedItems[0].Text + ".txt"));
                    stringToGrid(text);
                    switchScreen(Menus.Screen.Editor);
                }
                else
                {
                    ErrorLabel.Visible = true; ErrorLabel.Enabled = true;
                    ErrorLabel.Text = "La grille que vous essayez de charger n'existe plus.";
                    refreshSavesList();
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType() == typeof(InvalidGridSizeException))
                {
                    //Chargement échoué
                    ErrorLabel.Visible = true; ErrorLabel.Enabled = true;
                    ErrorLabel.Text = "La grille que vous essayez de charger est trop grande.";
                }
                else
                {
                    //Aucune sauvegarde sélectionnée
                    ErrorLabel.Visible = true; ErrorLabel.Enabled = true;
                    ErrorLabel.Text = "Aucune sauvegarde sélectionnée, veuillez sélectionner une sauvegarde.";
                }
            }
        }

        /// <summary>
        /// Evénement lancé à l'appui du bouton "Vider la grille".
        /// Supprime tous les virus présents sur la grille actuelle.
        /// </summary>
        /// <param name="sender">Bouton "Vider la grille"</param>
        /// <param name="e">Paramètres du bouton</param>
        private void EmptyGrid_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grid.Width; i++)
            {
                for (int j = 0; j < grid.Height; j++)
                {
                    grid.setValue(i, j, 0);
                    if (grid.Values[i, j] != lastGrid.Values[i, j])
                    {
                        //Colorie les cases qui étaient allumées
                        Color newColor;
                        newColor = Color.FromArgb(250, 250, 250);
                        using (Graphics g = Graphics.FromImage(MainBox.Image))
                        {
                            g.FillRectangle(new SolidBrush(newColor), i * 10 + 1, j * 10 + 1, 9, 9);
                        }
                    }
                    lastGrid.setValue(i, j, 0);
                }
            }
            selectedShape = -1;
            //Met à jour l'affichage
            Refresh();
        }
        #endregion

        #region Editor events
        /// <summary>
        /// Evénement lancé quand la grille est cliquée.
        /// Permet d'allumer ou éteindre une case de la grille selon le virus sélectionné.
        /// Permet également de placer les formes pré-définies sélectionnées.
        /// Uniquement dans l'éditeur.
        /// </summary>
        /// <param name="sender">Grille (Image)</param>
        /// <param name="e">Paramètres de la grille</param>
        private void MainBox_Click(object sender, MouseEventArgs e)
        {
            //Vérifie si l'on se trouve dans l'éditeur
            if (Menus.CurrentScreen == Menus.Screen.Editor)
            {
                if (selectedShape == -1)
                {
                    //Allume ou éteint une cellule selon le virus sélectionné
                    if (grid.Values[e.X / 10, e.Y / 10] == 0)
                    {
                        grid.setValue(e.X / 10, e.Y / 10, VirusChooser.SelectedIndex + 1);
                    }
                    else if (grid.Values[e.X / 10, e.Y / 10] == VirusChooser.SelectedIndex + 1)
                    {
                        grid.setValue(e.X / 10, e.Y / 10, 0);
                    }
                    else
                    {
                        grid.setValue(e.X / 10, e.Y / 10, VirusChooser.SelectedIndex + 1);
                    }
                    //Vérifie que l'état de la cellule ait bien changé
                    if (grid.Values[e.X / 10, e.Y / 10] != lastGrid.Values[e.X / 10, e.Y / 10])
                    {
                        //Colorie la cellule selon la couleur du virus présent dans la case
                        Color newColor;
                        newColor = virusList[grid.Values[e.X / 10, e.Y / 10]].Color;
                        using (Graphics g = Graphics.FromImage(MainBox.Image))
                        {
                            g.FillRectangle(new SolidBrush(newColor), (e.X / 10) * 10 + 1, (e.Y / 10) * 10 + 1, 9, 9);
                        }
                    }
                    //Met à jour les données et l'affichage
                    lastGrid.setValue(e.X / 10, e.Y / 10, grid.Values[e.X / 10, e.Y / 10]);
                    gridAlive.setValue(e.X / 10, e.Y / 10, 0);
                }
                else
                {
                    //Place la forme sélectionnée sur la grille
                    int xLocation;
                    int yLocation;
                    for (int i = -2; i <= 2; i++)
                    {
                        for (int j = -2; j <= 2; j++)
                        {
                            xLocation = e.X / 10 + i;
                            yLocation = e.Y / 10 + j;
                            //Met à jour les données et l'affichage
                            if (shapesGrid[selectedShape, i + 2, j + 2])
                            {
                                grid.setValue(xLocation, yLocation, VirusChooser.SelectedIndex + 1);
                                lastGrid.setValue(xLocation, yLocation, grid.Values[xLocation, yLocation]);
                                gridAlive.setValue(xLocation, yLocation, 0);
                                Color newColor;
                                newColor = virusList[grid.Values[xLocation, yLocation]].Color;
                                using (Graphics g = Graphics.FromImage(MainBox.Image))
                                {
                                    g.FillRectangle(new SolidBrush(newColor), xLocation * 10 + 1, yLocation * 10 + 1, 9, 9);
                                }
                            }
                        }
                    }
                    selectedShape = -1;
                }
                Refresh();
            }
        }

        /// <summary>
        /// Evénement lancé quand une valeur est sélectionnée dans la liste des virus
        /// Change la couleur des formes pré-définies dans l'éditeur pour qu'elles soient de la couleur du virus voulu
        /// </summary>
        /// <param name="sender">Liste déroulante des virus</param>
        /// <param name="e">Paramètres de la liste déroulante des virus</param>
        private void VirusChooser_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < ShapesPanel.Controls.Count; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    for (int k = 0; k < 5; k++)
                    {
                        //Colorie seulement les cases nécessaires
                        if (shapesGrid[i, j, k])
                        {
                            using (Graphics g = Graphics.FromImage(((PictureBox)ShapesPanel.Controls.Find("ShapePicture" + i, true)[0]).Image))
                            {
                                g.FillRectangle(new SolidBrush(virusList[VirusChooser.SelectedIndex + 1].Color), j * 10 + 1, k * 10 + 1, 9, 9);
                            }
                        }
                    }
                }
            }
            selectedShape = -1;
            //Met à jour l'affichage
            Refresh();
        }

        /// <summary>
        /// Evénement lancé quand l'on appuie sur une forme pré-définie
        /// Choisi la forme pré-définie à placer
        /// </summary>
        /// <param name="sender">Forme pré-définie dans l'éditeur</param>
        /// <param name="e">Paramètres de la forme pré-définie dans l'éditeur</param>
        private void ShapePicture_MouseClick(object sender, MouseEventArgs e)
        {
            if (Menus.CurrentScreen == Menus.Screen.Editor)
            {
                if (selectedShape != Int32.Parse(((PictureBox)sender).Name.Substring(12, ((PictureBox)sender).Name.Length - 12)))
                {
                    selectedShape = Int32.Parse(((PictureBox)sender).Name.Substring(12, ((PictureBox)sender).Name.Length - 12));
                }
                else
                {
                    selectedShape = -1;
                }
            }
        }

        /// <summary>
        /// Evénement lancé quand l'on apppuie sur le panneau des formes pré-définies
        /// Désélectionne la forme pré-définie sélectionnée
        /// </summary>
        /// <param name="sender">Panneau des formes pré-définies</param>
        /// <param name="e">Paramètres du panneau des formes pré-définies</param>
        private void ShapesPanel_MouseClick(object sender, MouseEventArgs e)
        {
            selectedShape = -1;
        }
        #endregion

        /// <summary>
        /// Evénement lancé quand la liste des sauvegardes apparaît ou disparaît.
        /// Permet de mettre à jour la liste des sauvegardes.
        /// </summary>
        /// <param name="sender">Liste de sauvegardes</param>
        /// <param name="e">Paramètres de la liste des sauvegardes</param>
        private void SavesList_VisibleChanged(object sender, EventArgs e)
        {
            refreshSavesList();
        }

        /// <summary>
        /// Evénement lancé quand une valeur est sélectionnée dans la liste des tailles d'écran.
        /// Egalement lancé quand il y a un changement entre "fenêtré" et "plein écran".
        /// Adapte la fenêtre à la taille voulue et au mode plein écran si souhaité.
        /// Adapte également les boutons du menu d'options au moment des changements.
        /// </summary>
        /// <param name="sender">Liste déroulante ou boutons radio des paramètres</param>
        /// <param name="e">Paramètres de la liste déroulante ou boutons radio des paramètres</param>
        private void ScreenSize_Changed(object sender, EventArgs e)
        {
            if (WindowedRadio.Checked)
            {
                //Passe en mode fenêtré
                FormBorderStyle = FormBorderStyle.FixedSingle;
                TopMost = false;
                Size = new Size(Int32.Parse(ComboScreenSize.SelectedItem.ToString().Split('x')[0]) + 16, Int32.Parse(ComboScreenSize.SelectedItem.ToString().Split('x')[1]) + 39);
                ComboScreenSize.Enabled = true;
            }
            else if (FullRadio.Checked)
            {
                //Passe en mode plein écran
                FormBorderStyle = FormBorderStyle.None;
                Location = new Point(0, 0);
                TopMost = true;
                Screen currentScreen = Screen.FromHandle(Handle);
                Size = new Size(currentScreen.Bounds.Width, currentScreen.Bounds.Height);
                ComboScreenSize.Enabled = false;
                ComboScreenSize.SelectedItem = currentScreen.Bounds.Width + "x" + currentScreen.Bounds.Height;
            }
            //Adapte l'emplacement des boutons selon l'état actuel de la fenêtre.
            int widthWindow = Int32.Parse(ComboScreenSize.SelectedItem.ToString().Split('x')[0]);
            int heightWindow = Int32.Parse(ComboScreenSize.SelectedItem.ToString().Split('x')[1]);
            QuitButton.Location = new Point(widthWindow / 2 + QuitButton.Width, heightWindow / 2 + QuitButton.Height);
            OptionsButton.Location = new Point(widthWindow / 2 - OptionsButton.Width, heightWindow / 2 + OptionsButton.Height);
            WindowedRadio.Location = new Point(widthWindow / 2 - WindowedRadio.Width, heightWindow / 2 - WindowedRadio.Height * 3);
            FullRadio.Location = new Point(widthWindow / 2 + FullRadio.Width, heightWindow / 2 - FullRadio.Height * 3);
            ComboScreenSize.Location = new Point((widthWindow - ComboScreenSize.Width) / 2, heightWindow / 2 - ComboScreenSize.Height);
        }
    }
}
