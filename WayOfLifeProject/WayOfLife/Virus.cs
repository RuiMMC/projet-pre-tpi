﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace WayOfLife
{
    /// <summary>
    /// Classe contenant chaque type de virus, leur nom et leur couleur.
    /// </summary>
    public class Virus
    {
        private Color color;
        public Color Color { get { return this.color; } }
        private string name;
        public string Name { get { return this.name; } }
        private int value;
        public int Value { get { return value; } }

        public Virus(string name, Color color, int value)
        {
            this.name = name;
            this.color = color;
            this.value = value;
        }
    }
}
