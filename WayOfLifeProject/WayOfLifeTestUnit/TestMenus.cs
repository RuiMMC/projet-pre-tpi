﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WayOfLife
{
    /// <summary>
    /// Classe de test des méthodes de la classe statique "Menus".
    /// </summary>
    [TestClass]
    public class TestMenus
    {
        private bool expectedPause;
        private Menus.Screen expectedCurrentScreen;
        private Menus.Screen expectedLastScreen;

        [TestInitialize]
        public void Initialize()
        {
            expectedPause = false;
            expectedCurrentScreen = Menus.Screen.None;
            expectedLastScreen = Menus.Screen.None;
        }

        /// <summary>
        /// Teste le changement de menu.
        /// </summary>
        [TestMethod]
        public void TestSwitchScreen()
        {
            expectedCurrentScreen = Menus.Screen.Save;
            expectedLastScreen = Menus.Screen.Pause;
            Menus.SwitchScreen(Menus.Screen.Pause);
            Menus.SwitchScreen(Menus.Screen.Save);

            Menus.Screen actualCurrentScreen = Menus.CurrentScreen;
            Menus.Screen actualLastScreen = Menus.LastScreen;

            Assert.AreEqual(expectedCurrentScreen, actualCurrentScreen);
            Assert.AreEqual(expectedLastScreen, actualLastScreen);
        }

        /// <summary>
        /// Teste la mise en pause.
        /// </summary>
        [TestMethod]
        public void TestPause()
        {
            expectedPause = true;
            Menus.PauseUpdate();

            bool actualPause = Menus.Pause;

            Assert.AreEqual(expectedPause, actualPause);
        }

        /// <summary>
        /// Teste la sortie de pause.
        /// </summary>
        [TestMethod]
        public void TestUnpause()
        {
            expectedPause = false;
            Menus.UnpauseUpdate();

            bool actualPause = Menus.Pause;

            Assert.AreEqual(expectedPause, actualPause);
        }

        [TestCleanup]
        public void Cleanup()
        {
            expectedPause = false;
            expectedCurrentScreen = Menus.Screen.None;
            expectedLastScreen = Menus.Screen.None;
        }
    }
}
