﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WayOfLife
{
    /// <summary>
    /// Classe de test des méthodes de la classe "Grid".
    /// </summary>
    [TestClass]
    public class TestGrid
    {
        private Grid testGrid;
        private int expectedWidth;
        private int expectedHeight;
        private string expectedString;
        private int[,] expectedGrid;
        private int expectedValue;

        [TestInitialize]
        public void Initialize()
        {
            testGrid = null;
            expectedWidth = -1;
            expectedHeight = -1;
            expectedString = null;
            expectedGrid = null;
            expectedValue = -1;
        }

        /// <summary>
        /// Teste l'instanciation de la classe virus.
        /// </summary>
        [TestMethod]
        public void TestCreateGrid()
        {
            expectedWidth = 10;
            expectedHeight = 12;
            testGrid = new Grid(expectedWidth, expectedHeight);

            int actualWidth = testGrid.Width;
            int actualHeight = testGrid.Height;

            Assert.AreEqual(expectedWidth, actualWidth);
            Assert.AreEqual(expectedHeight, actualHeight);
        }

        /// <summary>
        /// Teste la conversion correcte de la grille vers un string.
        /// </summary>
        [TestMethod]
        public void TestConvertGridToStringSuccess()
        {
            expectedString = "010\n000\n000\n";
            testGrid = new Grid(3, 3);
            testGrid.setValue(0, 1, 1);

            string actualString = testGrid.ToString();

            Assert.AreEqual(expectedString, actualString);
        }

        /// <summary>
        /// Teste la création d'une exception quand la grille est vide.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(EmptyGridException))]
        public void TestConvertGridToStringEmptyGridException()
        {
            testGrid = new Grid(3, 3);

            string exceptionLauncher = testGrid.ToString();
        }

        /// <summary>
        /// Teste la conversion correcte d'un string vers la grille.
        /// </summary>
        [TestMethod]
        public void TestLoadGridFromStringSuccess()
        {
            expectedGrid = new int[,]
            {
                { 0, 0, 1 },
                { 0, 1, 0 },
                { 1, 1, 0 }
            };
            testGrid = new Grid(3, 3);
            testGrid.loadGrid("001\n010\n110\n");

            int[,] actualGrid = testGrid.Values;

            CollectionAssert.AreEqual(expectedGrid, actualGrid);
        }

        /// <summary>
        /// Teste la création d'une exception quand la grille fournie dans le string est trop grande.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidGridSizeException))]
        public void TestLoadGridFromStringInvalidGridSizeException()
        {
            testGrid = new Grid(3, 3);
            testGrid.loadGrid("0010\n0201\n0010\n0001\n");
        }

        /// <summary>
        /// Teste l'utilisation de la méthode SetValue().
        /// </summary>
        [TestMethod]
        public void TestSetGridValue()
        {
            expectedValue = 9;
            testGrid = new Grid(3, 3);
            testGrid.setValue(2, 2, expectedValue);

            int actualValue = testGrid.Values[2, 2];

            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestCleanup]
        public void Cleanup()
        {
            testGrid = null;
            expectedWidth = -1;
            expectedHeight = -1;
            expectedString = null;
            expectedGrid = null;
            expectedValue = -1;
        }
    }
}
