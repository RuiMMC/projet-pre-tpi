﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

namespace WayOfLife
{
    /// <summary>
    /// Classe de test des méthodes de la classe "Virus".
    /// </summary>
    [TestClass]
    public class TestVirus
    {
        private Virus testVirus;
        private string expectedName;
        private Color expectedColor;
        private int expectedValue;

        [TestInitialize]
        public void Initialize()
        {
            testVirus = null;
            expectedName = null;
            expectedColor = Color.Empty;
            expectedValue = -1;
        }

        /// <summary>
        /// Teste l'instanciation de la classe virus.
        /// </summary>
        [TestMethod]
        public void TestCreateVirus()
        {
            expectedName = "Virus de test";
            expectedColor = Color.AliceBlue;
            expectedValue = 3;
            testVirus = new Virus(expectedName, expectedColor, expectedValue);

            string actualName = testVirus.Name;
            Color actualColor = testVirus.Color;
            int actualValue = testVirus.Value;

            Assert.AreEqual(expectedName, actualName);
            Assert.AreEqual(expectedColor, actualColor);
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestCleanup]
        public void Cleanup()
        {
            testVirus = null;
            expectedName = null;
            expectedColor = Color.Empty;
            expectedValue = -1;
        }
    }
}
